console.log("Hello world")

/*
	threre are 2 types of comments
	1. single line comment denoted by 2 clashes
	2. multiple line comment denoted by /* (ctrl sht /)
*/

// [Section] syntax, statements
	// statements in programming are instructions that we tell the computer to perform
	// JS statements usually end with semicolon (;)
	// semicolons are not required in JS but we will use it to help us train to locate where statement end
	// A syntax in programming, it is the set of rules that we describes statements must be constracted
	// all lines/blocks of codes should be written in specific manner to work. This is due to how these codes where initially programmed to function in a certain manner.

// [Section] Variable
	//Variables are used to contain data
	//Any information that is uded by an applicataion is stored in what we call the memory
	//when we create variables, certain portions of device's memory is given a name that we call variables
	//this makes it easier for us to associate information stored in our devices to actual "names" information
	// declaring variables - it tells our devices that a variable name is created and is ready to store data
		
		//Syntax:

			/* let/const variableName;*/

		let myVariable = "Ada Lovelace";
		//const keyword is use when the value of the variable won't change 
		const constVariable = "John Doe";
		// console.log is useful for printing values of variables or certain results of code into the browser's console
		let variable;
		console.log(variable);
		//this will caused an undefined variable because the declared variable does not have initial value
		console.log(myVariable);

		/*
			Guides in writing varibles:
				1. Use the let keyword followed the variable name of your choose and use the assignment operator (=)  to assign value.
				2. Variable names should start with lowercase character, use camelCasing for the multiple words.
				3. for constant variables, use the 'const' keyword
					Note: if we use const keyword in declaring a variable, we cannot change the value of its variable
				4. Variable names should be indicative(descriptive) of the value being stored to avoid confusion.
		*/


		// Declare and initialize 
			// initializing variables - the instance when a variable is given its first or initial value
			// Syntax:
				// let/cons variableName = initial value


			//declaration and initialization of the variable occur
			let productName = "desktop computer"
			//declaration
			let desktopName;
			console.log(desktopName)
			//initialization of the value of variable desktopName
			desktopName="Dell";
			console.log(desktopName)

			// reassigning value
				// syntax: variableName = newValue
			productName = "Personal Computer"
			console.log(productName)

			const name = "Chris"
			console.log(name);
			// this reassignment will cause an error since we cannot change  or reassign the initial value of constant value
			/*name = "Topher";
			console.log(name)*/

			//this will cause an erro on our code because the product name varialbe is already taken
				// let productName = "Laptop"

			// var vs let and const
				// some of you may wonder why we used let and const keyword in declaring a variable when we search online, we usually see var.

				// var is also used in declaring variable but var is an ecmaScript1 feature [(1997)].
				
			/*

			Scope - essentially means were these variable are available for use

			let/cons are block sope
			
			A block is a chunk of code bounded by {}.


			*/

	// let outerVariable = "Hello";
	// {
	// 	let innerVariable = "Hello Again"
	// 	// mag eerror to
	// }

	// console.log(outerVariable);
	// console.log(innerVariable);


	var outerVariable = "hello";
	{
		var  innerVariable = "Hello Again";
	}
	console.log(outerVariable);
	console.log(innerVariable);


	// Multiple variable declaration

	let productCode = "DC017", productBrand = "Dell";
	console.log(productCode);
	console.log(productBrand);


	//using a variable with a reserved keyword will cause an error
	// console let = "Hello"


	// data types:
	/*
		STrings are series of characters that create a word, a phrase , a sentence or anything related to creating a text
		Strings in JavaScript can be written usingeither a single quote or double quote
	*/

	let country = "Philippines";
	let province = 'Metro Manila'
	console.log(country);
	console.log(province);


	// concatenate string
	// multiple string values can be combined to create a single string using the + symbol

	let fullAddress = province + ", " + country
	console.log(fullAddress);

	let greeting = "I live in the " + country;
	console.log(greeting);

	let message = 'John\'s emloyee went home early'
	console.log(message);

	// \n refers to creating a new line in between texts

	// numbers
	// integer / whole number
	let headcount = 27;
	console.log(headcount);

	let grade = 98.7
	console.log(grade)

	let planetDistance = 2e10;
	console.log(planetDistance);


	let headcount1 = 27;
	console.log(typeof headcount1);

	let grade1 = 98.7
	console.log(typeof grade1)

	let planetDistance1 = 2e10;
	console.log(typeof planetDistance1);


	// boolean - values are normally used to store values realtating to the state of  certain things.
	// true or false
	let isMarried = false;
	let isGoodConduct = true;
	console.log(isMarried);
	console.log(isGoodConduct);

	// Arrays
	// arrays are a special kind data type that's use to store multiple values

	/*
		Syntax:
		let/const arrayName [elementA, elementB ...]
	*/


	//simmilar data types
	let grades = [98,92.1,90.1,94.4];
	console.log(grades);
	console.log(typeof grades)
	// array is a special kind of object


	//different data types
	let details = ["John", 32, true];
	console.log(details); 

	// objects are another special kind of data type thats used to mimid real workd object or items
	/*
		syntax:
			let/const objectName = {
				propertyA: value,
				propertyB: value
			}
	*/

	let person = {
		firstName: "John",
		lastName: "Smith",
		age: 32,
		isMarried: false,
		contact: ["+09123456789", "8123-4567"],
		address: {
			houseNumber: "345",
			city: "Manila"
		}
	}
	console.log(person);


	/*
		constant objects and arrays
		we can change the element of an array to a constant variable
	
	*/

	const anime = ["one piece", "one punch man", "your lie in april"];
	console.log(anime);
	anime[0] = ["kimetsu no yaba"];
	console.log(anime);

	// const anime1 = ["one piece", "one punch man", "your lie in april"];
	// console.log(anime1);
	// anime1 = ["kimetsu no yaba"];
	// console.log(anime1);


	// null
	// it is used to intentionally express the absence of a value in a variable declaration or initialization

	let spouse = null;
	console.log(spouse);


	// undefined represent the state of a variable that has been declared but without an assigned value

	let fullName;
	console.log(fullName);